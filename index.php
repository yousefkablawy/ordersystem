
<!DOCTYPE html>
<html lang="en">
<?php
  include "templates/header.php";
  include "config/connect_db.php";
  if(!$_SESSION['username'])
  {
    header('Location:login.php');
  }
  if($_SESSION['username']==='Admin')
  {
    echo "<a href='add.php' class='btn btn-primary mx-2'>add product</a>";
  }
  if(isset($_POST['btn']))
    {
        $productName=$_POST['productName'];
        $sql="SELECT * FROM products where product_name='$productName' ";
        $result=mysqli_query($conn,$sql);
        $products=mysqli_fetch_all($result,MYSQLI_ASSOC);
        mysqli_free_result($result);
        mysqli_close($conn);   
    }
  else
  {
      $sql='SELECT * FROM products';
      $result=mysqli_query($conn,$sql);
      $products=mysqli_fetch_all($result,MYSQLI_ASSOC);
      mysqli_free_result($result);
      mysqli_close($conn);
  }
?>

<div class="row">
  <?php foreach($products as $product): ?>
  <div class="col-lg-4">
    <div class="card mb-3" style="width: 18rem;">
      <img src="<?php echo "upload/".htmlspecialchars($product['filename'])?>" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title"><?php echo htmlspecialchars($product['product_name'])?></h5>
        <p class="card-text"><?php echo htmlspecialchars($product['product_desc'])?></p>
        <?php if($_SESSION['username']==='Admin'):?>

                    <a href="delete.php?id=<?php echo  htmlspecialchars($product['product_id'])?>" class='btn btn-danger mx-2'>Delete</a>
                    <a href="update.php?id=<?php echo htmlspecialchars($product['product_id'])?>"class='btn btn-success mx-2'>Edit</a>
        <?php else :?>  
                  <form action="myorder.php" method="post">
                      <input type="hidden" name='productId' value="<?php echo  htmlspecialchars($product['product_id'])?>">

                      <input type="number" name="quantity">
                      <button type='submit' name='send_order' class='btn btn-danger'>Order</button>
                  </form>
        <?php endif;?>
      </div>
    </div>
  </div>
  <?php endforeach;?>
</div>




<?php include "templates/footer.php"?>
</html>