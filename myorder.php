


<!DOCTYPE html>
<html lang="en">
<?php include "templates/header.php"?><?php 
    include "config/connect_db.php";
    $userid=$_SESSION['userid'];
    if(isset($_POST['send_order']))
    {
        $id=$_POST['productId'];
        $productquantity=$_POST['quantity'];
        $sql="SELECT * FROM products where product_id=$id";
        $result=mysqli_query($conn,$sql);
        $products=mysqli_fetch_all($result,MYSQLI_ASSOC);
        mysqli_free_result($result);
        foreach($products as $product)
        {
            $productname=$product['product_name'];
            $productprice=$product['product_price'];
            $productdesc=$product['product_desc'];
            $productimage=$product['filename'];
        }
       
2
       
    }

        $sql="SELECT * FROM orders where user_id='$userid'";
        $result=mysqli_query($conn,$sql);
        $orders=mysqli_fetch_all($result,MYSQLI_ASSOC);
        mysqli_free_result($result);
        mysqli_close($conn);
    
?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Product name</th>
            <th>product price</th>
            <th>product desc</th>
            <th>product image</th>
            <th>product quantity</th>
            <th>Order Date</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($orders as $order):?>
                <tr>
                    <td><?php echo htmlspecialchars($order['product_name'])?></td>
                    <td><?php echo htmlspecialchars($order['product_price'])?></td>
                    <td><?php echo htmlspecialchars($order['product_desc'])?></td>
                    <td><img src="<?php echo "upload/".htmlspecialchars($order['filename'])?>" class='w-25'></td>
                    <td><?php echo htmlspecialchars($order['product_quantity'])?></td>
                    <td><?php echo htmlspecialchars($order['order_date'])?></td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<?php include "templates/footer.php"?>
</html>