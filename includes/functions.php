<?php 
function validateName($name ,$error)
{
            if(empty($name))
            {
                $error['fullname']="please Enter Your Full Name";
            }
            else
            {
                if(!preg_match("/^([a-zA-Z' ]+)$/",$name))
                {
                    $error['fullname']="please Enter a Valid Full Name";
                }
            }
}

function validateUserName($username ,$error)
{
            if(empty($username))
                {
                    $error['username']="please Enter a username";
                }
            else
                {
                    if(!preg_match("/^([a-zA-Z0-9'])*$/",$username))
                        {
                            $error['username']="please Enter a Valid username";
                        }
                }
}
function validateEmail($email ,$error)
{
            if(empty($email))
                {
                    $error['email']="please Enter Your Email";
                }
            else
                {
                    if(!filter_var($email,FILTER_VALIDATE_EMAIL))
                    {
                        $error['email']="please Enter a Valid Email";
                    }
                }
}

function validatePassword($pwd,$error)
{
    if(empty($pwd))
            {
                $error['pwd']="please Enter a Password";
            }
    else
            {
                // Validate password strength
                $uppercase = preg_match('@[A-Z]@', $pwd);
                $lowercase = preg_match('@[a-z]@', $pwd);
                $number    = preg_match('@[0-9]@', $pwd);
                $specialChars = preg_match('@[^\w]@', $pwd);

                if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($pwd) < 8)
                {
                    $error['pwd'] ='Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.';
                }
            }
}

function validateConfirmPassword($pwd,$confirmpwd,$error)
{
    if(empty($confirmpwd))
            {
                $error['confirmpwd']="please Confirm You password";
            }
    else
            {
                if($pwd!==$confirmpwd)
                {
                    $error['confirmpwd']="the passwords dont match";
                }
            }
}

function userExist($conn,$username,$email,$error)
        {
            // check if username or email exists
                $sql=" SELECT * FROM users WHERE username=? OR email=? ";

                $stmt=mysqli_stmt_init($conn);

                if(!mysqli_stmt_prepare($stmt,$sql))
                {
                    $error['stmtfailed']='something went wrong try again';
                    header('Location:signup.php');
                    exit();
                }

                mysqli_stmt_bind_param($stmt,"ss",$username,$email);

                mysqli_stmt_execute($stmt);

                $resultData=mysqli_stmt_get_result($stmt);

                if($row=mysqli_fetch_assoc($resultData))
                {
                    return $row;
                }
                else
                {
                    return false;
                }
                mysqli_stmt_close($stmt);
        }


function createUser($conn,$fullname,$username,$email,$pwd,$error)
        {
            $sql="INSERT INTO users(fullname,username,email,pwd) VALUES (?,?,?,?) ;";
            $stmt=mysqli_stmt_init($conn);

            if(!mysqli_stmt_prepare($stmt,$sql))
                {
                    $error['stmtfailed']='something went wrong try again';
                    header('Location:signup.php');
                    exit();
                }
            $hashed_pwd=password_hash($pwd,PASSWORD_DEFAULT);
            mysqli_stmt_bind_param($stmt,"ssss",$fullname,$username,$email,$hashed_pwd);

            mysqli_stmt_execute($stmt);

        }      

function loginUser($conn,$username,$pwd,$error)
    {
        $userExist=userExist($conn,$username,$username,$error);
        if($userExist===false)
        {
            $error['userExist']="this user doesnt exist";
        }
        else
        {
            $hashed_pwd=$userExist['pwd'];
            
            if(!password_verify($pwd,$hashed_pwd))
            {
                $error['pwd']="this password doesnt match";
            }

            else
            {
                session_start();
                $_SESSION['username']=$userExist['username'];
                $_SESSION['userid']=$userExist['id'];
                header('Location:index.php');
                exit();
            }
        }
        return $error;

    }
?>