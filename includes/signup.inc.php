<?php   
        include "functions.php";
        $error=['fullname'=>'','email'=>'', 'pwd'=>'', 'confirmpwd'=>'' , 'username'=>'','userExist'=>'','stmtfailed'=>'','none'=>''];
        $fullname=$email=$pwd=$confirmpwd=$username='';
        if(isset($_POST['btn']))
        {
            // put input into variable
            $fullname=$_POST['fullname'];
            $email=$_POST['email'];
            $pwd=$_POST['pwd'];
            $username=$_POST['username']; 
            $confirmpwd=$_POST['confirmpwd']; 
            // check if any input field is empty and validate the input

            //fullname
            validateName($fullname,$error);

            //username
            validateUserName($username,$error);

            //email
            validateEmail($email,$error);

            //password
            validatePassword($pwd,$error);

            //confirm password
            validateConfirmPassword($pwd,$confirmpwd,$error);

            // chech if there are no errors and if the user is exist
            if(!array_filter($error))
            {   
                // check if user already exsist
                if(userExist($conn,$username,$email,$error))
                    {
                        $error['userExist']='sorry this user is already exists';
                        
                    }
                else
                    {
                        createUser($conn,$fullname,$username,$email,$pwd,$error);
                        $error['none']="You are Signed up!!";
                        
                    }
            }
        }

        
        
    
?>