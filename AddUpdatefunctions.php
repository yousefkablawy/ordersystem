<?php 
function validateFileName($fileName,$fileTempName,$fileSize,$fileError,$fileExt,$fileActualExt,$allowedExt,$error)
{
      if(!empty($fileName))
        {
            if(in_array($fileActualExt,$allowedExt))
                {
                    if($fileError===0)
                    {
                        if($fileSize< 5000000)
                        {
                            $fileDestination="upload/".$fileName;
                            $uploadImage=move_uploaded_file($fileTempName,$fileDestination);
                            if(!$uploadImage)
                            {
                                $error['productimage']="sorry this is an error while uploading image";
                            }
                        }
                        else
                        {
                        $error['productimage']="this image is too big";
                        }
                    }
                    else
                    {
                        $error['productimage']="there was a problem uploading this file";
                    }

                }
            else
                {
                    $error['productimage']="the extension of an image must be jpg,jpeg,png";
                }
        }
    else
        {
            $error['productimage']="you must insert an image";
        }
}

function validateProductName($productname ,$error)
{
            if(empty($productname))
                {
                    $error['productname']="please Enter a productname";
                }
            else
                {
                    if(!preg_match("/^([a-zA-Z0-9])*$/",$productname))
                        {
                            $error['productname']="please Enter a Valid productname";
                        }
                }
}
function validateProductPrice($productprice ,$error)
{
            if(empty($productprice))
                {
                    $error['productprice']="please Enter a productprice";
                }
            else
                {
                    if(!preg_match("/^([0-9])*$/",$productprice))
                        {
                            $error['productprice']="please Enter a Valid productprice";
                        }
                }
}
function validateProductDesc($productdesc ,$error)
{
        if(empty($productdesc))
                    {
                        $error['productdesc']="please Enter a productdesc";
                    }
        else
            {
                    if(!preg_match("/^([a-zA-Z ])*$/",$productdesc))
                    {
                        $error['productdesc']="please Enter a Valid productdescription";
                    }
            }
}
function addProduct($conn,$productname,$productprice,$productdesc,$fileName,$error)
{
            $sql="INSERT INTO products (product_name,product_price,product_desc,filename) VALUES (?,?,?,?) ;";
            $stmt=mysqli_stmt_init($conn);

            if(!mysqli_stmt_prepare($stmt,$sql))
                {
                    $error['stmtfailed']='something went wrong try again';
                    header('Location:add.php');
                    exit();
                }
            mysqli_stmt_bind_param($stmt,"sdss",$productname,$productprice,$productdesc,$fileName);
            mysqli_stmt_execute($stmt);

}


function updateProduct($conn,$productname,$productprice,$productdesc,$fileName,$error,$id)
{
                // productimage
                validateFileName($fileName,$fileTempName,$fileSize,$fileError,$fileExt,$fileActualExt,$allowedExt,$error);
                if($error['productimage']==="you must insert an image")
                {
                    $sql="SELECT * FROM products WHERE product_id=$id";
                    $result=mysqli_query($conn,$sql);
                    $product=mysqli_fetch_assoc($result);
                    $fileName=$product['filename'];
                    mysqli_free_result($result);
                    
                }
                //productname
                validateProductName($productname,$error);
                //productprice
                validateProductPrice($productprice,$error);
                //productdesc
                validateProductDesc($productdesc,$error);
                        
                if(empty($error['productname']) && empty($error['productprice']) && empty($error['productdesc']))
                {
                    $sql= "UPDATE products SET product_name='$productname',product_price=$productprice,product_desc='$productdesc',filename='$fileName'WHERE product_id=$id ;";
                    if(mysqli_query($conn,$sql))
                    {
                        header('Location:index.php');
                    }
                } 
}
?>