<?php session_start();?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-2">
  <div class="container">
    <a class="navbar-brand" href="index.php">E-shop</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="index.php">Home</a>
        </li>>
        <?php 
        if(!isset($_SESSION['userid']))
        {
          echo " <li class='nav-item'><a class='nav-link' href='login.php'>Login</a></li>";
          echo " <li class='nav-item'><a class='nav-link' href='signup.php'>signup</a></li>";
        }
        else
        {
          echo "<li class='nav-item'><a class='nav-link' href='logout.php'>Logout</a></li>";
          if($_SESSION['username']!=='Admin')
          {
            echo "<li class='nav-item'><a class='nav-link' href='myorder.php'>My Order</a></li>";
          }
        }
      
?>

      </ul>
      <form method="post" action="index.php"class="d-flex">
        <input name="productName" class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="submit" name="btn">Search</button>
      </form>
    </div>
  </div>
</nav>

<div class="container">


