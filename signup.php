<?php 
  // make a connection to database
    include "config/connect_db.php"; 
    include "includes/signup.inc.php"

?>
<!DOCTYPE html>
<html lang="en">
<?php include "templates/header.php"?>
<form action="signup.php" method="POST">
    <?php if($error['none']):?>
            <div class='alert alert-success mt-2'><?php echo htmlspecialchars($error['none']);?></div>
    <?php endif;?>
<div class="mb-3">
    <label  class="form-label">Full Name:</label>
    <input type="text" name="fullname" class="form-control" >
    <?php if($error['fullname']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['fullname'])?></div>
    <?php endif;?>
</div>
<div class="mb-3">
    <label  class="form-label">Email address</label>
    <input type="text" name="email" class="form-control">
    <?php if($error['email']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['email']);?></div>
    <?php endif;?>
</div>
<div class="mb-3">
    <label  class="form-label">UserName:</label>
    <input type="text" name="username" class="form-control" >
    <?php if($error['username']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['username']);?></div>
    <?php endif;?>
</div>
<div class="mb-3">
    <label  class="form-label">Password</label>
    <input type="password" name="pwd" class="form-control"  >
    <?php if($error['pwd']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['pwd']);?></div>
    <?php endif;?>
</div>
<div class="mb-3">
    <label  class="form-label">Confirm Password</label>
    <input type="password" name="confirmpwd" class="form-control" >
    <?php if($error['confirmpwd']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['confirmpwd']);?></div>
    <?php endif;?>
</div>
<?php if($error['stmtfailed']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['stmtfailed']);?></div>
<?php endif;?>
<?php if($error['userExist']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['userExist']);?></div>
<?php endif;?>

<button type="submit" class="btn btn-primary" name='btn'>Submit</button>
</form>
<?php include "templates/footer.php"?>
</html>