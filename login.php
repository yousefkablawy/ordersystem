<?php
  // make a connection to database
    include "config/connect_db.php"; 
    include "includes/login.inc.php";
?>
<!DOCTYPE html>
<html lang="en">
<?php include "templates/header.php"?>
<form action="login.php" method="post">
  <div class="mb-3">
    <label  class="form-label">Email address OR email</label>
    <input type="text" name="username" class="form-control" placeholder='Username\email : ....'>
    <?php if($error['username']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['username']);?></div>
    <?php endif;?>
  </div>
  <div class="mb-3">
    <label  class="form-label">Password</label>
    <input type="password" name="pwd" class="form-control" >
    <?php if($error['pwd']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['pwd']);?></div>
    <?php endif;?>
  </div>
  <button type="btn" name="btn" class="btn btn-primary">Submit</button>
<?php if($error['stmtfailed']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['stmtfailed']);?></div>
<?php endif;?>
<?php if($error['userExist']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['userExist']);?></div>
<?php endif;?>
</form>
<?php include "templates/footer.php"?>
</html>