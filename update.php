
<?php
    include 'config/connect_db.php';
    include 'AddUpdatefunctions.php';
?>
<!DOCTYPE html>
<html lang="en">
<?php
    include "templates/header.php";
    $id=$_GET['id'];
    $error=['productname'=>'','productprice'=>'','productdesc'=>'','productimage'=>'','none'=>''];
    $productname=$productprice=$productdesc="";
    if(isset($_POST['btn']))
    {   
                $productname=$_POST['productname'];
                $productprice=$_POST['productprice'];
                $productdesc=$_POST['productdesc'];
                
                $fileName = $_FILES["image"]["name"];
                $fileTempName = $_FILES["image"]["tmp_name"];    
                $fileSize= $_FILES["image"]["size"];
                $fileError=$_FILES["image"]["error"];
                $fileExt=explode('.',$fileName);
                $fileActualExt=strtolower(end($fileExt));
                $allowedExt=array('jpg','jpeg','png');
                updateProduct($conn,$productname,$productprice,$productdesc,$fileName,$fileTempName,$fileSize,$fileError,$fileExt,$fileActualExt,$allowedExt,$error,$id);
    }
    else
    {
    $sql="SELECT * FROM products WHERE product_id='$id'";
    $result=mysqli_query($conn,$sql);
    $product=mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    mysqli_close($conn);
    }
?>
<form action="update.php" method="POST" enctype="multipart/form-data">
  <div class="mb-3">
    <label  class="form-label">Enter the product name</label>
    <input type="text" name="productname" class="form-control" value="<?php echo htmlspecialchars($product['product_name'])?>">
    <?php if($error['productname']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['productname']);?></div>
    <?php endif;?>
  </div>
  <div class="mb-3">
    <label  class="form-label">Enter the product price</label>
    <input type="text" name="productprice" class="form-control" value="<?php echo htmlspecialchars($product['product_price'])?>">
    <?php if($error['productprice']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['productprice']);?></div>
    <?php endif;?>
  </div>
  <div class="form-group">
  <label>Product description</label>
  <textarea class="form-control" rows="5" name='productdesc'><?php echo htmlspecialchars($product['product_desc']);?></textarea>
</div>
<div class="mb-3">
    <label  class="form-label">Insert an image</label>
    <input type="file" name="image" class="form-control">
    <?php if($error['productdesc']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['productdesc']);?></div>
  <?php endif;?>
  </div>
  <a href="index.php" class='btn btn-danger' > Cancel</a>
<button type='submit' name='btn' class='btn btn-info my-1'>update</button>
</form>
<?php include "templates/footer.php"?>
</html>