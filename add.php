<?php
    include "config/connect_db.php";
    include 'AddUpdatefunctions.php';
  ?>
<!DOCTYPE html>
<html lang="en">
<?php
    include "templates/header.php";
    $error=['productname'=>'','productprice'=>'','productdesc'=>'','productimage'=>'','none'=>'','stmtfailed'=>''];
    $productname=$productprice=$productdesc="";
    if(isset($_POST['btn']))
    {
                $productname=$_POST['productname'];
                $productprice=$_POST['productprice'];
                $productdesc=$_POST['productdesc'];
                // image handling error
                $fileName = $_FILES["image"]["name"];
                $fileTempName = $_FILES["image"]["tmp_name"];    
                $fileSize= $_FILES["image"]["size"];
                $fileError=$_FILES["image"]["error"];
                $fileExt=explode('.',$fileName);
                $fileActualExt=strtolower(end($fileExt));
                $allowedExt=array('jpg','jpeg','png');
                // IMAGE VALIDATE
                validateFileName($fileName,$fileTempName,$fileSize,$fileError,$fileExt,$fileActualExt,$allowedExt,$error);
                //productname
                validateProductName($productname ,$error);
                //productprice
                validateProductPrice($productprice ,$error);
                //productdesc
                validateProductDesc($productdesc ,$error);
                // after check if all input is filled and valid
                if(!array_filter($error))
                {
                    addProduct($conn,$productname,$productprice,$productdesc,$fileName,$error);
                    $error['none'] ='you are added a new product';
                }
                
    }
?>
<form action="add.php" method="post" enctype="multipart/form-data">
  <div class="mb-3">
    <label  class="form-label">Enter the product name</label>
    <input type="text" name="productname" class="form-control" value="<?php echo htmlspecialchars($productname)?>">
    <?php if($error['productname']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['productname']);?></div>
    <?php endif;?>
  </div>
  <div class="mb-3">
    <label  class="form-label">Enter the product price</label>
    <input type="text" name="productprice" class="form-control" value="<?php echo htmlspecialchars($productprice)?>">
    <?php if($error['productprice']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['productprice']);?></div>
    <?php endif;?>
  </div>
  <div class="form-group">
  <label>Product description</label>
  <textarea class="form-control" rows="5" name='productdesc'> <?php echo htmlspecialchars($productdesc)?> </textarea>
  <?php if($error['productdesc']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['productdesc']);?></div>
  <?php endif;?>
</div>
<div class="mb-3">
    <label  class="form-label">Insert an image</label>
    <input type="file" name="image" class="form-control">
    <?php if($error['productimage']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($error['productimage']);?></div>
    <?php endif;?>
  </div>
<button type='submit' name='btn' class='btn btn-info my-1'>submit</button>
  <?php if($error['none']):?>
            <div class='alert alert-success mt-2'><?php echo htmlspecialchars($error['none']);?></div>
    <?php endif;?>
</form>
<?php include "templates/footer.php"?>
</html>